<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
     public function change_status($id,$status)
     {
        return view('tasks.index', ['id'=>$id, 'status'=>$status]);
     }    
    
     public function index()
    {
        $tasks = Task::all();
        return view('tasks.index', compact('tasks'));
    }

    public function indexFiltered()
    {
        $id = $id = Auth::id(); //the current user 
        $tasks = Task::where('user_id', $id)->get();
        $filtered = 1; //this is to mark the view to show link to all tasks 
        return view('tasks.index', compact('tasks','filtered'));
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tasks.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Task();
        $id = Auth::id(); //the idea of the current user
        $task->title = $request->title;
        $task->user_id = $id;
        $task->status = 0;
        $task->save();
        return redirect('tasks');         //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::findOrFail($id);
        return view('tasks.edit', compact('task'));      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //only if this todo belongs to user 
        $task = Task::findOrFail($id);            
        $task->update($request->except(['_token'])); 
        return redirect('tasks');    
    }

    public function done($id)
    {
        //only if this todo belongs to user 
        if (Gate::denies('admin')) {
            abort(403,"You are not allowed to mark tasks as dome..");
         }          
        $task = Task::findOrFail($id);            
        $task->status = 1; 
        $task->save();
        return redirect('tasks');    
    }    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {     
        if (Gate::denies('admin')) {
           abort(403,"You are not allowed to delete tasks..");
        }  
        $task = Task::findOrFail($id);
        $task->delete();
        return redirect('tasks');        //
    }
}
