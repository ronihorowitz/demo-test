@extends('layouts.app')

@section('content')
<h1>Create a new task!</h1>
<form method="post" action="{{action('TaskController@store')}}">
@csrf
          <div class="form-group">    
              <label for="title">What is the task?</label>
              <input type="text" class="form-control" name="title"/>
          </div>
          <div class="form-group">    
              <input type="submit" class="btn btn-primary" name="submit" value = "save todo"/>
          </div>          
</form> 
@endsection